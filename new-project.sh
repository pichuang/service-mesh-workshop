#!/bin/bash
export OCP_TUTORIAL_PROJECT=$USER_ID-tutorial
oc new-project $OCP_TUTORIAL_PROJECT
oc adm policy add-scc-to-user anyuid -z default -n $OCP_TUTORIAL_PROJECT --as=system:admin
oc adm policy add-scc-to-user privileged -z default -n $OCP_TUTORIAL_PROJECT --as=system:admin
mkdir ~/lab && cd "$_"
git clone https://github.com/gpe-mw-training/ocp-service-mesh-foundations
cd ocp-service-mesh-foundations
cd ~/lab/ocp-service-mesh-foundations/catalog
oc create -f kubernetes/catalog-service-template.yml -n $OCP_TUTORIAL_PROJECT
oc create -f kubernetes/Service.yml -n $OCP_TUTORIAL_PROJECT
cd ~/lab/ocp-service-mesh-foundations/partner
oc create -f kubernetes/partner-service-template.yml -n $OCP_TUTORIAL_PROJECT
oc create -f kubernetes/Service.yml -n $OCP_TUTORIAL_PROJECT
cd ~/lab/ocp-service-mesh-foundations/gateway
oc create -f kubernetes/gateway-service-template.yml -n $OCP_TUTORIAL_PROJECT
oc create -f kubernetes/Service.yml -n $OCP_TUTORIAL_PROJECT
oc expose service gateway
sleap 3
echo "export GATEWAY_URL=http://$(oc get route gateway -n $OCP_TUTORIAL_PROJECT -o template --template='{{.spec.host}}')" > ~/.bashrc
source ~/.bashrc
echo $GATEWAY_URL
curl $GATEWAY_URL
cd ~/lab/ocp-service-mesh-foundations/catalog-v2
oc create -f kubernetes/catalog-service-template.yml -n $OCP_TUTORIAL_PROJECT

oc project $OCP_TUTORIAL_PROJECT
oc get pods -l application=catalog -w